use actix_service::{Service, Transform};
use actix_web::{
    dev::ServiceRequest,
    dev::ServiceResponse,
    http::header::{HeaderValue, CACHE_CONTROL},
    Error,
};
// use actix_http::http::header::Expires;
use futures::{
    future::{ok, Ready},
    Future,
};

use std::pin::Pin;
use std::task::{Context, Poll};

pub struct CacheForYear;

impl<S, B> Transform<S, ServiceRequest> for CacheForYear
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = MyCacheInterceptorMiddleware<S>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(MyCacheInterceptorMiddleware { service })
    }
}

/// Cache static files for 1 year.
///
/// cache-control: max-age=31536000, public, must-revalidate
///
/// "public" - files can be cached in user's browsers AND proxies. Only
/// for public static files.
///
/// "max-age=31536000" - 1 year
///
/// "must-revalidate" - once a resource becomes stale, caches must not
/// use their stale copy without successful validation on the origin
/// server
pub struct MyCacheInterceptorMiddleware<S> {
    service: S,
}

impl<S, B> Service<ServiceRequest> for MyCacheInterceptorMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

    fn poll_ready(&self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let fut = self.service.call(req);

        Box::pin(async move {
            let mut res = fut.await?;
            let headers = res.headers_mut();
            // headers.append(
            //     EXPIRES,
            //     HeaderValue::from_static("Thu, 31 Dec 2037 23:55:55 GMT"),
            // );
            headers.append(
                CACHE_CONTROL,
                HeaderValue::from_static("max-age=31536000, public, must-revalidate"),
            );
            return Ok(res);
        })
    }
}
