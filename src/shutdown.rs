use tokio::sync::broadcast;
use tokio::signal::unix::{signal, SignalKind};

/// Listens for the server shutdown signal.
///
/// Shutdown is signalled using a `broadcast::Receiver`. Only a single value is
/// ever sent. Once a value has been sent via the broadcast channel, the server
/// should shutdown.
///
/// The `Shutdown` struct listens for the signal and tracks that the signal has
/// been received. Callers may query for whether the shutdown signal has been
/// received or not.
#[derive(Debug)]
pub struct Shutdown {
    /// `true` if the shutdown signal has been received
    shutdown: bool,

    /// The receive half of the channel used to listen for shutdown.
    notify: broadcast::Receiver<()>,
}

impl Shutdown {
    /// Create a new `Shutdown` backed by the given `broadcast::Receiver`.
    pub fn new(notify: broadcast::Receiver<()>) -> Shutdown {
        Shutdown {
            shutdown: false,
            notify,
        }
    }

    /// Returns `true` if the shutdown signal has been received.
    pub fn is_shutdown(&self) -> bool {
        self.shutdown
    }

    /// Receive the shutdown notice, waiting if necessary.
    pub async fn recv(&mut self) {
        // If the shutdown signal has already been received, then return
        // immediately.
        if self.shutdown {
            return;
        }

        // Cannot receive a "lag error" as only one value is ever sent.
        let _ = self.notify.recv().await;

        // Remember that the signal has been received.
        self.shutdown = true;
    }
}


/// How to terminate a program?
///
/// 1. SIGINT - user can press Ctrl-C.
///
/// 2. SIGTERM - systemd or `docker-compose` can send it.  By default
/// sends a SIGTERM, followed by 90 seconds (for systemd) of waiting
/// followed by a SIGKILL.
pub async fn graceful_shutdown() {
    // SIGINT (Ctrl-c)
    let mut sigint = signal(SignalKind::interrupt()).unwrap();

    // SIGTERM
    let mut sigterm = signal(SignalKind::terminate()).unwrap();

    // Wait for SIGINT or SIGTERM
    tokio::select! {
        _ = sigint.recv() => {
            // warn!("c-c");
        }
        _ = sigterm.recv() => {
            // warn!("term");
        }
    };
}
