//! My API

use actix_web::{HttpRequest, HttpResponse, Responder};
// use actix_web::body::BoxBody;
use actix_web::body::EitherBody;
// use actix_web::error::UrlencodedError;

pub mod error;
pub use error::Errors;

pub mod form;
pub use form::Form;

// use actix_web::{Error};
// use actix_web::HttpResponse;
use serde::Serialize;
// use hashbrown::HashMap;

/// API response
#[derive(Serialize)]
pub struct Response<'a> {
    pub errors: Errors<'a>,
    // pub errors: Vec<&'a str>,
}


impl Response<'_> {
    pub fn new() -> Self {
        Self {
            errors: Errors::new(),
        }
    }

    // pub fn actix(&self) -> Result<HttpResponse, Error> {
    //     Ok(HttpResponse::Ok().json(self))
    // }
}

// impl Responder for Response<'_> {
//     // type Item = Reply;
//     // type Error = Error;
//     type Body = String;

//     #[inline]
//     fn respond_to(self, req: &HttpRequest) -> HttpResponse<Self::Body> {
//         // "self".into()
//         // let res: actix_http::Response<_> = "string".into();
//         HttpResponse::from("string".to_string())
//     }
// }

impl Responder for Response<'_> {
    type Body = EitherBody<String>;

    fn respond_to(self, _: &HttpRequest) -> HttpResponse<Self::Body> {
        match serde_json::to_string(&self) {
            Ok(body) => match HttpResponse::Ok()
                .content_type("application/json")
                .message_body(body)
            {
                Ok(res) => res.map_into_left_body(),
                Err(err) => HttpResponse::from_error(err).map_into_right_body(),
            },

            Err(err) => {
                HttpResponse::from_error(err).map_into_right_body()
                // HttpResponse::from_error(UrlencodedError::Serialize(err)).map_into_right_body()
            }
        }
    }
}
