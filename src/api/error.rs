//! API response errors
use serde::{Serialize, Serializer};
use hashbrown::HashMap;
use serde::ser::SerializeMap;

pub struct Errors<'a> {
    pub errors: HashMap<&'a str, String>,
}

impl<'a> Errors<'a> {
    pub fn new() -> Self {
        Self {
            errors: HashMap::new(),
        }
    }

    pub fn push(&mut self, k: &'a str, v: String) {
        self.errors.insert(k, v);
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.errors.len()
    }
}

impl<'a> Serialize for Errors<'a> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(self.errors.len()))?;
        for (k, v) in &self.errors {
            map.serialize_entry(&k.to_string(), &v)?;
        }
        map.end()
    }
}


// pub struct Errors<A, B> {
//     pub errors: HashMap<A, B>,
// }

// impl<A, B> Errors<A, B> {
//     pub fn new() -> Self {
//         Self {
//             errors: HashMap::new(),
//         }
//     }

//     pub fn push(&mut self, k: A, v: B) {
//         self.errors.insert(k, v);
//     }

//     #[inline]
//     pub fn len(&self) -> usize {
//         self.errors.len()
//     }
// }
