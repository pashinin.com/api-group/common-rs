//!

// use hashbrown::HashMap;
use super::Errors;

pub trait Form {
    // fn errors(&self) -> Option<HashMap<&str, &str>>;
    fn errors(&self) -> Errors;
}
