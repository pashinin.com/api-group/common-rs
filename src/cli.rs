//! CLI

use atty::Stream;
use std::fs;

/// Tells if this terminal was opened by a user.
/// Taken from Termion project.
pub fn can_open_terminal() -> bool {
    // This part used .unwrap() and errored sometimes (when running
    // 'cargo watch ...')
    //
    // https://github.com/redox-os/termion/blob/master/src/async.rs#L14
    if let Err(_) = fs::OpenOptions::new().read(true).write(true).open("/dev/tty") {
        // info!(log, "Can't start terminal: {}", e);
        // info!(log, "Are you are running this binary inside other process?");
        return false
    }

    atty::is(Stream::Stdout) && atty::is(Stream::Stdin)
}
