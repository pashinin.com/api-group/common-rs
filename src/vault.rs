//! Some functions to access Hashicorp Vault

use hashicorp_vault as vault;

/// Try to get a secret from Vault
pub fn vault_get(key: &str) -> Option<String> {
    let f = match home::home_dir() {
        Some(path) => {
            std::path::Path::new(&path).join(".vault-token")
        },
        None => return None,
    };
    let token = match std::fs::read_to_string(&f) {
        Ok(token) => token,
        Err(_e) => {
            // error!("Can't read {}: {}", f.display(), e);
            return None;
        }
    };

    let host = "http://vault.service.consul:8200";
    let client = vault::Client::new(host, token).unwrap();

    // let _ = client.set_secret("foo", "{\"bar\": \"baz\"}");
    match client.get_secret(key) {
        Ok(v) => Some(v),
        Err(_e) => {
            None
        }
    }
}
