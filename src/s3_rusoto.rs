use std::env;
use rusoto_core::{HttpClient, Region};
use rusoto_s3::{S3Client};

use native_tls::TlsConnector;
// use tokio_native_tls::TlsConnector;
// use tokio_tls::TlsConnector;

use hyper::client::HttpConnector;
use hyper_tls::HttpsConnector;
use rusoto_credential::{
    DefaultCredentialsProvider
    // EnvironmentProvider,
    // ProvideAwsCredentials,
    // AwsCredentials,
    // StaticProvider
};
use tracing::info;

#[cfg(not(debug_assertions))]
static S3_ENDPOINT: &'static str = "https://s3.pashinin.com";

#[cfg(debug_assertions)]
static S3_ENDPOINT: &'static str = "http://minio:9000";


pub async fn connect_insecure(endpoint: String) -> S3Client {
    // 1. create tls connector which accepts invalid certs
    // For native-tls
    let tls_connector: TlsConnector = TlsConnector::builder()
        .danger_accept_invalid_certs(true)
        .build()
        .expect("failed to build tls connector");

    // For tokio_tls
    // let ttc = tokio_tls::TlsConnector::from(tls_connector);
    let ttc = tokio_native_tls::TlsConnector::from(tls_connector);

    // 2. create http connector - make sure to not enforce http
    let mut http_connector = HttpConnector::new();
    http_connector.enforce_http(false);

    // 3. create https connector
    // let https_connector = HttpsConnector::from((http_connector, tls_connector));
    let https_connector = HttpsConnector::from((http_connector, ttc));
    // let https_connector = HttpsConnector::new_(tls_connector);

    // 4. create rusoto http client to be used in rusoto services
    let http_client = HttpClient::from_connector(https_connector);

    let cred_provider = DefaultCredentialsProvider::new().expect("failed to create cred provider");

    // custom region to talk to local service with a self-signed cert
    let local_region = Region::Custom {
        name: "us-east-1".to_owned(),
        endpoint: endpoint.to_owned(),
    };

    // 5. initialize service clients with new http client
    let s3_client = S3Client::new_with(http_client, cred_provider, local_region);
    s3_client
}

/// Creates S3 client using env vars
///
/// * S3_ENDPOINT
///
/// Rusoto env variables:
///
/// * AWS_ACCESS_KEY_ID
/// * AWS_SECRET_ACCESS_KEY
pub async fn connect() -> S3Client {
    let endpoint = env::var("S3_ENDPOINT").unwrap_or(S3_ENDPOINT.to_string());
    let access_key = env::var("AWS_ACCESS_KEY_ID").unwrap_or("".to_string());
    let secret_key = env::var("AWS_SECRET_ACCESS_KEY").unwrap_or("".to_string());
    env::set_var("AWS_ACCESS_KEY_ID", access_key);
    env::set_var("AWS_SECRET_ACCESS_KEY", secret_key);
    env::set_var("S3_ENDPOINT", endpoint.clone());

    info!("S3 endpoint: {}", endpoint);
    if endpoint.contains("localhost") || endpoint.contains("127.0.0.1") {
        connect_insecure(endpoint).await
    } else {
        S3Client::new(Region::Custom {
            name: "us-east-1".to_owned(),
            endpoint: endpoint.to_owned()
        })
    }
}
