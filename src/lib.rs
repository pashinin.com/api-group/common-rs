use actix_cors::Cors;
use actix_web::{HttpRequest};

#[cfg(not(debug_assertions))]
use actix_web::http;

pub mod api;
pub mod cli;
pub mod middlewares;

#[cfg(feature = "s3")]
pub mod s3;

pub mod sql;
pub mod shutdown;

#[cfg(feature = "hashicorp_vault")]
pub mod vault;


#[cfg(not(debug_assertions))]
pub static DOMAIN: &'static str = "pashinin.com";

#[cfg(debug_assertions)]
pub static DOMAIN: &'static str = "pashinin.localhost";


/// Returns a part of a hostname
///
/// Examples:
///
/// Returns "localhost" for "localhost:8000" and "localhost".
///
/// Returns "baumanka" from hostname like "baumanka."
pub fn get_hostname_from_request(req: &HttpRequest) -> String {
    let hostname: String = match req.headers().get("host") {
        Some(host) => host.to_str().unwrap().to_string(),
        _ => return "".into(),
    };
    if hostname.starts_with("localhost:") {
        return "localhost".into();
    }
    let pos = hostname.find("-latest.");
    if let Some(pos) = pos {
        return (hostname[0..pos]).to_string();
    }
    hostname
}


/// CORS for my services.
///
/// Debug mode - everything is allowed.
///
/// Release mode, allowed:
///   * only requests from .pashinin.com.
///   * GET, POST, OPTIONS methods
pub fn cors() -> Cors {
    // Debug
    #[cfg(debug_assertions)]
    return Cors::permissive();

    // Release
    #[cfg(not(debug_assertions))]
    return Cors::default()
        .allowed_origin("https://pashinin.com")
        .allowed_origin_fn(|origin, _req_head| {
            origin.as_bytes().ends_with(b".pashinin.com")
        })
        .allowed_methods(vec!["GET", "POST", "OPTIONS"])
        .allowed_headers(vec![
            http::header::AUTHORIZATION,
            http::header::ACCEPT,
            http::header::CONTENT_TYPE,
        ])
        .supports_credentials()  // to be able to submit cookies in API
        .max_age(3600);
}
