//! Functions to connect to database
//!
//! ```text
//! postgres://postgres@10.254.239.5:5432
//! postgres://yugabyte@10.254.239.5:5433
//! postgres://postgres@postgres:5432       # dev
//! ```

use sqlx::postgres::PgPoolOptions;
use std::env;
use std::time::Duration;
use tracing::{error, info};


#[cfg(not(debug_assertions))]
static PG_HOST: &'static str = "10.254.239.3";

#[cfg(debug_assertions)]
static PG_HOST: &'static str = "postgres";


/// Gets PGPASSWORD
///
/// 1. Try PGPASSWORD variable
/// 2. Try to get it from Vault
/// 3. Use default: "password"
pub fn get_db_pass() -> String {
    env::var("PG_PASSWORD").unwrap_or(
        // Disabled vault module due to a problem with building in CI.
        // error[E0659]: `parse_quote_spanned` is ambiguous
        // pin-project-internal-0.4.27
        //
        // It is a dep of hashicorp_vault
        //
        // match crate::vault::vault_get("PG_POSTGRES_PASSWORD") {
        //     Some(v) => v,
        //     None => "password".into(),
        // }

        // crate::vault::vault_get("PG_POSTGRES_PASSWORD").unwrap_or("password".into())
        "password".into()
    )
}

/// DB hostname
///
/// In Release mode: "10.254.239.3"
/// In Debug mode: "postgres"
pub fn get_db_host() -> String {
    env::var("PG_HOST")
        // .unwrap_or(
        //     crate::vault::vault_get("PG_HOST")
        .unwrap_or(PG_HOST.into())
}

pub fn get_db_user() -> String {
    env::var("PG_USER").unwrap_or("postgres".into())
}


/// Connects to a PostgreSQL server.
///
/// Defaults in debug mode: `PGHOST="postgres", PGPASSWORD="password"`
pub async fn connect() -> Result<sqlx::Pool<sqlx::Postgres>, sqlx::Error> {
    let host = get_db_host();
    info!("Connecting to DB at {}", host);

    // Gets all info from env vars
    let connect_options = sqlx::postgres::PgConnectOptions::new()
        .host(&host)
        .port(5432)
        .username(&get_db_user())
        .password(&get_db_pass());

    // Connect
    PgPoolOptions::new()
        .max_connections(10)
        .acquire_timeout(Duration::new(5, 0))  // 5s, first connect is slow for YugabyteDB
        .connect_with(connect_options).await
        .map_err(|err| {
            error!("Can't connect to {}: {}", &host, err);
            error!("Set PG_HOST");
            error!("Set PG_USER");
            error!("Set PG_PASSWORD");

            #[cfg(feature = "sentry")]
            sentry::capture_error(&err);
            err
        })
        .and_then(|pool| {
            if pool.size() < 1 {
                let msg = format!(
                    "Can't acquire even one DB connection",
                );
                error!("{}", &msg);

                #[cfg(feature = "sentry")]
                sentry::capture_error(&sqlx::Error::Protocol(msg.clone()));

                return Err(sqlx::Error::Protocol(msg));
            }
            Ok(pool)
        })
}
